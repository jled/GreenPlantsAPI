# GreenPlants API Backend

Proyecto de Fin de Grado realizado para la titulación de Grado en Ingeniería Informática de la Facultad de Informática en la Universidad del País Vasco.

En él se realiza el diseño e implementación de en dispositivo IOT llamado GreenPlants. El
objetivo de éste es el de configurar y administrar un sistema de irrigación, o riego automático, a
distancia, para que el cliente pueda mantenerse informado del estado de sus plantas y puedan
regarse automáticamente a la vez que ahorra agua y se gana en eficacia.

Parte del proyecto que consiste en la API desarrollada usando __Node__, __Express__ y __MongoDB__.

## Running the server

Run `npm start` to start the API. Use `http://localhost:8000/`.