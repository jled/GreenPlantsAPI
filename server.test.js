// Plant API Server
// Se añade la Express y el middleware body-parser
const express = require('express')
const bodyParser = require('body-parser')
// Se añade la libreria config
const config = require ('config')
// Cargamos Express
const app = express()

// Cargamos el Middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.text())
app.use(bodyParser.json({ type: 'application/json'}))

app.get('/api/connection', async (req, res) => {
    let answer,status
    try {
        answer = await configuration.isInitialized()
    } catch(err) {
        answer = false
    }
    
    status = answer?200:204
    res.json({
        'status': status,
        'message': 'PlantAPI v' + config.VERSION, 
    })
})

if(process.env.NODE_ENV === 'test') {
    app.EXPRESS_APP = true
    module.exports = app; // Just for testing
} else {
    app.listen(config.API_PORT, () => console.log("Server up at http://localhost:" + config.API_PORT))
}
