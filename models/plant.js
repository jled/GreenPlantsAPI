let mongoose = require('mongoose')
let uniqueValidator = require('mongoose-unique-validator');
let exec = require('child_process').exec;

let Schema = mongoose.Schema

let conditionSchema = new Schema({
    enabled:    {type:Boolean, required:true, default: true},
    sensor:     {type:Number},
    condition:  {type:String},
    value:      {type: Number}
})

let timeConditionSchema = new Schema({
    value:      {type: Number, required: true},
    exceptions: [conditionSchema]
})

let sensorDataSchema = new Schema({
    date:   {type:Date, required: true},
    value:  {type:Number, required: true}
})

let plantSchema = new Schema({
    name:           {type:String, required: true, unique: true, minlength:3, maxlength:40},
    description:    {type:String, required: true, maxlength:250},
    servo:          {type:Number, required: true, unique: true},
    sensor_1:       {type:Number, required: true, default:-1}, // Humedad
    sensor_2:       {type:Number, required: true, default:-1}, // Temperatura
    sensor_1Data:   [sensorDataSchema],
    sensor_2Data:   [sensorDataSchema],
    ultimo_riego:   [{type:Date, required: true}],    // Data
    tiempo_abertura:{type:Number,required:true, default:2}, // Segundos
    caudal:         {type:Number,required:true, default:2},
    weatherAfected: {type:Boolean, required: true, default:false},
    maxLastOpening: {type:Number,required:true, default:30},
    timeConditions: timeConditionSchema,
    conditions:     [conditionSchema],
    photo:          {type:String, required:true, default:"http://res.cloudinary.com/greenplants/image/upload/sample.jpg"},
    enabled:        {type:Boolean, required:true, default: true},
})

plantSchema.plugin(uniqueValidator)

// Python readings
plantSchema.pre('save', function (next) {
    exec("readings --humedad " + this.servo, (err, stdout, stderr) => {
        if(err || stderr) {
            this.sensor_1 = -1;
            this.sensor_2 = -1;
            next()
        }
        this.sensor_1 = parseInt(stdout, 10)
        exec("readings --temperatura " + this.servo, (err, stdout, stderr) => {
            if(err || stderr) {
                this.sensor_2 = -1;
                next()
            }
            this.sensor_2 = parseInt(stdout, 10)
            this.sensor_1Data.push({
                date: new Date(),
                value: this.sensor_1
            })
            this.sensor_2Data.push({
                date: new Date(),
                value: this.sensor_2
            })
            next()
        })
    })
});
*/
module.exports = mongoose.model('plant', plantSchema)
