const plant = require('../models/plant')

// For weather API
const configuration = require('../models/configuration')
const http = require('http')
const https = require('https')

// For Reset
let mongoose = require('mongoose')

function getPlants(req, res) {
    let query = plant.find({$query : {}, $orderby: { servo: 1 }})
    query.exec((err, plants) => {
        if (err) {
            return res.json({
                status: 500,
                error: err
            })
        } else {
            res.json({
                status: 200,
                plants
            })
        }

    })
}

function getPlant(req, res) {
    let query = plant.find({_id: req.params.id})
    query.exec((err, plant) => {
        if (err) {
            return res.json({
                status: 500,
                error: err
            })
        } else {
            res.json({
                status: 200,
                plant
            })
        }

    })
}

function postPlant(req, res) {
    console.log(req.body);
    let newPlant = new plant(req.body)
    newPlant.save((err, plant) => {
        if (err) {
            return res.json({
                status: 500,
                error: err
            })
        }
        res.json({
            status: 200,
            data: plant
        })
    })
}

function patchPlant(req, res) {
    plant.findById(req.params.id, (err, plantEdited) => {
        if (err) {
            if (err.name === "CastError")
                return res.json({
                    status: 404,
                    error: "Not Found"
                })
            return res.json({
                status: 500,
                error: err
            })
        }
        for (var index in req.body) {
            if (index === "_id" && plantEdited._id != req.params.id) {
                return res.json({
                    status: 500,
                    error: "Cant update object ID"
                })
            }
            plantEdited[index] = req.body[index]
        }
        plantEdited.save((err, result) => {
            if (err)
                return res.json({
                    status: 500,
                    error: err
                })
            res.json({
                status: 200,
                message: result
            })
        })
    })
}

function deletePlant(req, res) {
    plant.findById(req.params.id, (err, plantDeleted) => {
        if (err) {
            if (err.name === "CastError")
                return res.json({
                    status: 404,
                    error: "Not Found"
                })
            return res.json({
                status: 500,
                error: err
            })
        }
        plantDeleted.remove((err, result) => {
            if (err)
                return res.json({
                    status: 500,
                    error: err
                })
            res.json({
                status: 200,
                message: result
            })
        })
    })
}

function getWeather(req, res) {
    // For Wunderground API
    const requestOptionsWunderground = {
        host: 'api.wunderground.com',
        path: '/api/' + require('config').WUNDERGROUND_API_KEY + '/conditions/q/'
    }
    //

    // For google Maps API
    const requestOptionsGoogleMaps = {
        host: 'maps.googleapis.com',
        path: '/maps/api/geocode/json?key=' + require('config').GOOGLEMAPS_API_KEY + '&latlng='
    }
    //
    let location
    configuration.find((err, config) => {
        if (err) {
            return res.json({
                status: 500,
                error: "Error with the configuration"
            })
        }
        location = config[0].location;
        requestOptionsWunderground.path += location + '.json'
        requestOptionsGoogleMaps.path += location
        let chunks = []
        let requestW = http.get(requestOptionsWunderground, (responseW) => {
            responseW.on('data', (chunk) => {
                chunks.push(chunk)
            })
            responseW.on('end', () => {
                let dataLocation
                let dataWeather = JSON.parse(Buffer.concat(chunks).toString('utf8')).current_observation;
                chunks = []
                let requestGM = https.get(requestOptionsGoogleMaps, (responseGM) => {
                    responseGM.on('data', (chunk) => {
                        chunks.push(chunk)
                    })
                    responseGM.on('end', () => {
                        dataLocation = JSON.parse(Buffer.concat(chunks).toString('utf8')).results['0'].address_components
                        res.json({
                            status: 200,
                            dataLocation,
                            dataWeather
                        })
                    })
                    responseGM.on('error', () => {
                        res.json({
                            status: 500,
                            data: "Error in the GoogleMaps response"
                        })
                    })
                })
                requestGM.on('error', (err) => {
                    res.json({
                        status: 500,
                        data: "Error in the GoogleMaps request: " + err
                    })
                })
            })
            responseW.on('error', () => {
                res.json({
                    status: 500,
                    data: "Error in the Wunderground response"
                })
            })
        })
        requestW.on('error', (err) => {
            res.json({
                status: 500,
                data: "Error in the Wunderground request: " + err
            })
        })
    })
}

function getConfiguration(req, res) {
    configuration.find({}, (err, config) => {
        if (err)
            return res.json({
                status: 500,
                error: err
            })
        res.json({
            status: 200,
            data: config[0]
        })
    })
}

function changeConfiguration(req, res) {
    configuration.findOne({}, (err, config) => {
        if (err)
            return res.json({
                status: 500,
                error: err
            })
        for (var index in req.body) {
            if (index === "_id") {
                return res.json({
                    status: 500,
                    error: "Cant update object ID"
                })
            }
            config[index] = req.body[index]
        }
        config.save((err, saved) => {
            if (err)
                return res.json({
                    status: 500,
                    error: "Error saving data: " + err
                })
            res.json({
                status: 200,
                data: saved
            })
        })
    })
}

function changePassword(req, res) {
    configuration.findOne({}, (err, config) => {
        if (err)
            return res.json({
                status: 500,
                error: err
            })
        if (!config.checkPassword(req.body.oldpassword)) {
            return res.json({
                status: 500,
                error: "Old Password do not match"
            })
        }
        config.password = req.body.newpassword
        config.save((err, saved) => {
            if (err)
                return res.json({
                    status: 500,
                    error: "Error saving data: " + err
                })
            res.json({
                status: 200,
                data: saved
            })
        })
    })
}

function resetConfiguration(req, res) {
    let ConfDrop = mongoose.connection.db.dropCollection("configurations", (err, conf) => {
        if(err)
            return res.json({
                status: 500,
                error: "Configuration Couldnt be dropped"
            })
        mongoose.connection.db.dropCollection('plants', (err, plant) => {
            if(err)
                return res.json({
                    status: 500,
                    error: "Plants Couldnt be dropped"
                })
            res.json({
                    status: 200
            })
        })
    });
}



module.exports = {
    getPlants,
    getPlant,
    postPlant,
    patchPlant,
    deletePlant,
    getWeather,
    getConfiguration,
    changeConfiguration,
    changePassword,
    resetConfiguration
}