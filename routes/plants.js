const express = require('express')
const router = express.Router()
const config = require('config')

const plant = require('../controllers/plant.js')

router.get('/plants', plant.getPlants)

router.post('/plants', plant.postPlant)

router.get('/plant/:id', plant.getPlant)

router.patch('/plant/:id', plant.patchPlant)

router.delete('/plant/:id', plant.deletePlant)

router.get('/weather', plant.getWeather)

router.get('/configuration', plant.getConfiguration)

router.post('/configuration', plant.changeConfiguration)

router.post('/password', plant.changePassword)

router.post('/reset', plant.resetConfiguration)

module.exports = router