// Se añade la app para comprobar
let app = require('./server.test')
//app = undefined
// Librerias de aserción
const chai = require('chai')
const chaiHttp = require('chai-http')
// Funciones de aserción
global.expect = chai.expect
global.should = chai.should()

chai.use(chaiHttp)

describe("Testing Básico de Configuración", function() {

    it('Debería existir la aplicación', function() {
        expect(app).to.not.be.an('undefined')
    })

    it('Debería ser una instancia de API EXPRESS', function() {
        expect(app.EXPRESS_APP).to.be.true
        expect(app.settings['x-powered-by']).to.be.true
        expect(app.get).to.be.a('function')
        expect(app.post).to.be.a('function')
        expect(app.put).to.be.a('function')
        expect(app.delete).to.be.a('function')
    })

    it('Debería devolver el nombre de la API más la version', function (done) {
        chai.request(app)
            .get('/api/connection')
            .end((err, res) => {
                expect(err).to.be.null
                res.should.not.be.null
                res.body.should.be.a('Object')
                const returnedMessage = res.body.message.split(' ')
                const v = returnedMessage[1][0]
                const version = returnedMessage[1].split('v')[1]
                const major = parseInt(version.split('.')[0])
                const minor = parseInt(version.split('.')[1])
                const patch = parseInt(version.split('.')[2])
                returnedMessage[0].should.be.eql('PlantAPI')
                v.should.be.eql('v')
                major.should.be.a('number')
                minor.should.be.a('number')
                patch.should.be.a('number')
                done()
            })
    })
})